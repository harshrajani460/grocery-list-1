import React from "react";
import GroceryListItem from "./GroceryListItem";
class Grocery extends React.Component{
    constructor({ util, list, setCurrForm }) {
        
    }
    render(){
        return (
            <div className="grocery">
                <div className="grocery-title">Grocery List</div>
                <div className="grocery-holder">
                    <ul className="grocery-list">
                        {list.map((listItem) => {
                            return (
                                <GroceryListItem
                                    listItem={listItem}
                                    util={util}
                                    key={listItem.itemId}
                                    // setCurrForm={setCurrForm}
                                />
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}
export default Grocery;
