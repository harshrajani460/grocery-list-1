import React from "react";
import form_util from "./form_util";

class Form extends React.Component {
    constructor({ util , setCurrForm}) {
        super();
        this.util = util;
    }
    componentDidMount() {
        form_util.attachELToForm(this.util);
    }
    render() {
        return (
            <div className="form-section">
                <div className="form-section-title">
                    <p>Add Item</p>
                </div>
                <div className="form-holder" id="form-holder">
                    <form action="">
                        <input
                            type="text"
                            name="itemName"
                            placeholder="Item Name"
                            id="inputItemName"
                        />
                        <input
                            type="number"
                            name="itemQuantity"
                            placeholder="Item Quantity"
                            id="inputItemQuantity"
                            min="1"
                        />
                        <input type="submit" id="inputItem" value="Add" />
                        <button className="cancelBtn hidden" type="button">
                            Cancel
                        </button>
                    </form>
                </div>
            </div>
        );
    }
}
export default Form;
