import React from "react";
import ReactDOM from "react-dom";
import grocery_util from "./grocery_util";
class GroceryListItem extends React.Component {
    constructor({ listItem, util}) {
        super();
        this.listItem = listItem;
        this.util = util;
        // this.setCurrForm = setCurrForm
    }
    componentDidMount() {
        grocery_util.attachELToGroceryListItem(
            ReactDOM.findDOMNode(this),
            this.util,
            // this.setCurrForm
        );
    }
    render() {
        return (
            <li
                className="grocery-list-item"
                itemname={this.listItem.itemName}
                itemquantity={this.listItem.itemQuantity}
            >
                <p className="name">{this.listItem.itemName}</p>
                <p className="quantity">x {this.listItem.itemQuantity}</p>
                <button className="edit">Edit</button>
                <button className="delete">Delete</button>
            </li>
        );
    }
}

export default GroceryListItem;
