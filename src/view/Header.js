import React from "react";

function Header() {
    return (
        <div className="header">
            <h1 className="title">My Grocery List</h1>
            <i className="fa fa-shopping-cart"></i>
        </div>
    );
}

export default Header;
